// نصب تایپ اسکریپت
// cmd => npm (node package manager) install typescript -g

//start 
// let firstName = 'farzad';
// console.log(firstName);
// export { }
// let fname: string = 'farzad';
// fname = 'keyvan';
// console.log(fname);


/*





// @errors: 2551  // جاوااسکریپت
const obj = { width: 10, height: 15 };
const area = obj;
console.log(obj.height * obj.width);

[LOG]: {
  "width": 10,
  "height": 15
} 
[LOG]: 10 
[LOG]: 15 
[LOG]: 150 


// @errors: 2551 // تایپ اسکریپت
const obj = { width: 10, height: 15 };
const area = obj.width * obj.heigth;

Errors in code  دلیل ارور : it is an error of using some kind of value (a type) in an incorrect way.
Property 'heigth' does not exist on type '{ width: number; height: number; }'. Did you mean 'height'?


console.log(4 / []); // :  infinity جاوا اسکریپت






console.log(4 / []); // main.ts:48:17 - error TS2363: The right-hand side of an arithmetic operation must be of type 'any', 'number', 'bigint' or an enum type.


// تایپ اسکریپت هرگز رفتار زمان اجرای کد جاوا اسکریپت رو تغییر نمیده
//it is guaranteed to run the same way, even if TypeScript thinks that the code has type errors.


// Accessing the property 'toLowerCase'
// on 'message' and then calling it
// const message = "Hello World!";

// message.toLowerCase();
// // Calling 'message'
// message();


// تی اس در مرورگر یا سرور اجرا نمیشه با استفاده از کامپایلری که داره تی اس رو به جی اس تبدیل میکنه و بعد فایل تبدیلی اجرا میشه

// خطاهایی که قرار بود در زمان اجرای جی اس داشته باشیم در زمان کامپایل مشخص میشن


const user = {  // جی اس
  name: "Daniel",
  age: 26,
};
user.location; // returns undefined

const user = {  // تی اس
  name: "Daniel",
  age: 26,
};

user.location;
Property 'location' does not exist on type '{ name: string; age: number; }'.


const obj = {
  name : 'farzad',
  age : 28
}
obj.location;

Property 'location' does not exist on type '{ name: string; age: number; }'.

// @noErrors
// This is an industrial-grade general-purpose greeter function:
function greet(person, date) {
  console.log(`Hello ${person}, today is ${date}!`);
}

greet("farzad");

Parameter 'person' implicitly has an 'any' type.
Parameter 'date' implicitly has an 'any' type.
Expected 2 arguments, but got 1.

tsc --noEmitOnError hello.ts


function greet(person: string, date: Date) {
  console.log(`Hello ${person}, today is ${date.toDateString()}!`);
}
greet('farzad',new Date());

[LOG]: "Hello farzad, today is Sat Apr 24 2021!" 

let msg = "hello there!";
//   با اینکه تایپ مسیج تعریف نشده تی اس خودکار استرینگ در نظر می گیرد

// فایل تی اس
// @showEmit
// @target: es5
function greet(person: string, date: Date) {
  console.log(`Hello ${person}, today is ${date.toDateString()}!`);
}

greet("Maddison", new Date());

// تبدیل شده به جی اس
"use strict";
// @showEmit
// @target: es5
function greet(person, date) {
    console.log("Hello " + person + ", today is " + date.toDateString() + "!");
}
greet("Maddison", new Date());





// مثال
<!DOCTYPE html >
    <html lang="en" >
        <head>
        <meta charset="UTF-8" />
            <meta name="viewport" content = "width=device-width, initial-scale=1.0" />
                <meta http - equiv="X-UA-Compatible" content = "ie=edge" />
                    <title>Understanding TypeScript < /title>
                        < script src = "using-ts.js" defer > </script>
                            < /head>
                            < body >
                            <input type="number" id = "num1" placeholder = "Number 1" />
                                <input type="number" id = "num2" placeholder = "Number 2" />
                                    <button>Add! < /button>
                                    < /body>
                                    < /html>

// جاوااسکریپت
var button = document.querySelector("button");
var input1 = document.getElementById("num1");
var input2 = document.getElementById("num2");
function add(num1, num2) {
    return num1 + num2;
}
button.addEventListener("click", function () {
    console.log(add(input1.value, input2.value));
});

//  input اول عدد 10 و در input دوم عدد 5  : 
// نتیجه میشه 105 نه 15 چون پیشفرض رشته هستن

// const input1 = document.getElementById("num1")! as HTMLInputElement;  علامت ! 

// مشخص کردن تایپ
function add(num1: number, num2: number) {
    return num1 + num2;
}

// تبدیل به عدد با +
button.addEventListener("click", function () {
    console.log(add(+input1.value, +input2.value));
});



// مفاهیم dynamic type و static type
//number :  -10و 5.6 و 6

//string :
// استفاده از علامت single quote (یعنی ') مانند 'Hi'
// استفاده از علامت double quote (یعنی ") مانند "Hi"
// استفاده از template literal (یعنی علامت backtic یا `) مانند `Hi`


function add(n1, n2) {
    return n1 + n2;
}

const number1 = 5;
const number2 = 2.8;

const result = add(number1, number2);
console.log(result);


const number1 = '5';
const number2 = 2.8;
// 52.8 

function add(n1: number, n2: number) { // فقط عدد
    return n1 + n2;
}

function add(n1: string, n2: string) {  // فقط رشته
    return n1 + n2;
}

let number1: number = 5;
let number1: number;
let number1: number;
number1 = '5';   // خطا


// ============================================

const person = {
    name: 'Maximilian',
    age: 30
};

console.log(person);

console.log(person.nickname); // خطا

// اضافه کردن تایپ شی
const person: object = {
    name: 'Maximilian',
    age: 30
};


const person: {} = {
    name: 'Maximilian',
    age: 30
};


// ============================================

const person = {
    name: 'Maximilian',
    age: 30,
    hobbies: ['Sports', 'Cooking']
};


const person = {
    name: 'Maximilian',
    age: 30,
    hobbies: ['Sports', 'Cooking']
};

let favoriteActivities: string[];  // تعریف تایپ آرایه ای از نوع رشته

//Tuple

const person = {
    name: 'Maximilian',
    age: 30,
    hobbies: ['Sports', 'Cooking'],
    role: [2, 'author']
};

const person: {
    name: string;
    age: number;
    hobbies: string[];
    role: [number, string]; // tuple تعریف 
} = {
    name: 'Maximilian',
    age: 30,
    hobbies: ['Sports', 'Cooking'],
    role: [2, 'author']
};

// enum
Enum name { NEW, OLD }

const person = {
    name: 'Maximilian',
    age: 30,
    hobbies: ['Sports', 'Cooking'],
    role: [2, 'author']
};


enum Role { ADMIN = 'ADMIN', READ_ONLY = 100, AUTHOR = 'AUTHOR' };

enum Role { ADMIN, READ_ONLY, AUTHOR };

enum Role { ADMIN = 5, READ_ONLY, AUTHOR };  // میتونیم ایندکس را خودمان تعیین کنیم


enum Role { ADMIN, READ_ONLY, AUTHOR };

const person = {
    name: 'Maximilian',
    age: 30,
    hobbies: ['Sports', 'Cooking'],
    role: Role.ADMIN
};


//Any

let favoriteActivities: any;
let favoriteActivities: any[]; // محدودتر کردیم گفتیم هر تایپی از نوع آرایه باشد


//Union Type

// مثال زیر فقط جمع اعداد رو قبول میکنه
function combine(input1: number, input2: number) {
    const result = input1 + input2;
    return result;
}

// میخوایم کاری کنیم هم با رشته هم عدد بتونه کار کنه

function combine(input1: number | string, input2: number | string) {
    const result = input1 + input2;
    return result;
}

function combine(input1: number | string, input2: number | string) {
    let result;
    if (typeof input1 === 'number' && typeof input2 === 'number') {
        result = input1 + input2;
    } else {
        result = input1.toString() + input2.toString();
    }
    return result;
}


//Literal Type

function combine(input1: number | string, input2: number | string, resultConversion: string) {
    const combinedAges = combine(30, 26, 'as-number');
    const combinedAges = combine(30, 26, 'as-number');
    console.log(combinedAges);

    const combinedStringAges = combine('30', '26', 'as-number');
    console.log(combinedStringAges);

    const combinedNames = combine('Max', 'Anna', 'as-text');
    console.log(combinedNames);

    function combine(input1: number | string, input2: number | string, resultConversion: string) {
        let result;
        if (typeof input1 === 'number' && typeof input2 === 'number') {
            result = input1 + input2;
        } else {
            result = input1.toString() + input2.toString();
        }
        if (resultConversion === 'as-number') {
            return +result;
        } else {
            return result.toString();
        }
    }


    //alias
    type Combinable = number;

    type Combinable = number | string;


    function combine(
        input1: number | string,
        input2: number | string,
        resultConversion: 'as-number' | 'as-text'
    ) {
        let result;
        if (typeof input1 === 'number' && typeof input2 === 'number' || resultConversion === 'as-number') {
            result = +input1 + +input2;
        } else {
            result = input1.toString() + input2.toString();
        }
        return result;
    }
    // تابع را ساده تر کردیم

    function combine(
        input1: Combinable,
        input2: Combinable,

        type ConversionDescriptor = 'as-number' | 'as-text';


        type Combinable = number | string;
        type ConversionDescriptor = 'as-number' | 'as-text';
        
        function combine(
            input1: Combinable,
            input2: Combinable,
            resultConversion: ConversionDescriptor
        ) {
        let result;
        // بقیه ی کد ها //



        type User = { name: string; age: number };
        const u1: User = { name: 'Max', age: 30 };

        function greet(user: { name: string; age: number }) {
            console.log('Hi, I am ' + user.name);
        }

        function isOlder(user: { name: string; age: number }, checkAge: number) {
            return checkAge > user.age;
        }



        type User = { name: string; age: number };

        function greet(user: User) {
            console.log('Hi, I am ' + user.name);
        }

        function isOlder(user: User, checkAge: number) {
            return checkAge > user.age;
        }

        //Function Type ها و callback ها

        function add(n1: number, n2: number): number {
            return n1 + n2;
        }

        function printResult(num: number): void {
            console.log('Result: ' + num);


            let combineValues;

            combineValues = add;

            let combineValues;

            combineValues = add;
            console.log(combineValues(8, 8)); // مقدار 16

            let combineValues;

            combineValues = add;
            combineValues = 5;
            console.log(combineValues(8, 8));


            let combineValues: Function;

            combineValues = add;
            combineValues = 5;
            console.log(combineValues(8, 8));


            let combineValues: Function;

            combineValues = add;
            combineValues = printResult;
            console.log(combineValues(8, 8));

            // Result: 8
            //undefined

            // کلاس ها

            class Department {

            }

            class Department {
                name: string;

                constructor(n: string) {
                    this.name = n;
                }
            }


            const accounting = new Department('Accounting');


            class Department {
                name: string;

                constructor(n: string) {
                    this.name = n;
                }

                describe() {
                    console.log('Department: ' + name);
                }
            }

            describe() {
                console.log('Department: ' + this.name);
            }


            class Department {
                name: string;

                constructor(n: string) {
                    this.name = n;
                }

                describe() {
                    console.log('Department: ' + this.name);
                }
            }

            const accounting = new Department('Accounting');

            accounting.describe();



            const accounting = new Department('Accounting');

            accounting.describe();

            const accountingCopy = { describe: accounting.describe };

            accountingCopy.describe();


            class Department {
                name: string;
              
                constructor(n: string) {
                  this.name = n;
                }
              
                describe(this: Department) {
                  console.log('Department: ' + this.name);
                }
              }



              class Department {
                name: string;
              
                constructor(n: string) {
                  this.name = n;
                }
              
                describe(this: Department) {
                  console.log('Department: ' + this.name);
                }
              }
              
              const accounting = new Department('Accounting');
              
              accounting.describe();
              
              const accountingCopy = { name: 'DUMMY', describe: accounting.describe };
              
              accountingCopy.describe();


              */
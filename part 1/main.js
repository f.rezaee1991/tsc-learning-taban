// نصب تایپ اسکریپت
// cmd => npm (node package manager) install typescript -g
//start 
// let firstName = 'farzad';
// console.log(firstName);
// export { }
// let fname: string = 'farzad';
// fname = 'keyvan';
// console.log(fname);
/*





// @errors: 2551  // جاوااسکریپت
const obj = { width: 10, height: 15 };
const area = obj;
console.log(obj.height * obj.width);

[LOG]: {
  "width": 10,
  "height": 15
}
[LOG]: 10
[LOG]: 15
[LOG]: 150


// @errors: 2551 // تایپ اسکریپت
const obj = { width: 10, height: 15 };
const area = obj.width * obj.heigth;

Errors in code  دلیل ارور : it is an error of using some kind of value (a type) in an incorrect way.
Property 'heigth' does not exist on type '{ width: number; height: number; }'. Did you mean 'height'?


console.log(4 / []); // :  infinity جاوا اسکریپت






console.log(4 / []); // main.ts:48:17 - error TS2363: The right-hand side of an arithmetic operation must be of type 'any', 'number', 'bigint' or an enum type.


// تایپ اسکریپت هرگز رفتار زمان اجرای کد جاوا اسکریپت رو تغییر نمیده
//it is guaranteed to run the same way, even if TypeScript thinks that the code has type errors.


// Accessing the property 'toLowerCase'
// on 'message' and then calling it
// const message = "Hello World!";

// message.toLowerCase();
// // Calling 'message'
// message();


// تی اس در مرورگر یا سرور اجرا نمیشه با استفاده از کامپایلری که داره تی اس رو به جی اس تبدیل میکنه و بعد فایل تبدیلی اجرا میشه

// خطاهایی که قرار بود در زمان اجرای جی اس داشته باشیم در زمان کامپایل مشخص میشن


const user = {  // جی اس
  name: "Daniel",
  age: 26,
};
user.location; // returns undefined

const user = {  // تی اس
  name: "Daniel",
  age: 26,
};

user.location;
Property 'location' does not exist on type '{ name: string; age: number; }'.


const obj = {
  name : 'farzad',
  age : 28
}
obj.location;

Property 'location' does not exist on type '{ name: string; age: number; }'.

// @noErrors
// This is an industrial-grade general-purpose greeter function:
function greet(person, date) {
  console.log(`Hello ${person}, today is ${date}!`);
}

greet("farzad");

Parameter 'person' implicitly has an 'any' type.
Parameter 'date' implicitly has an 'any' type.
Expected 2 arguments, but got 1.

tsc --noEmitOnError hello.ts


function greet(person: string, date: Date) {
  console.log(`Hello ${person}, today is ${date.toDateString()}!`);
}
greet('farzad',new Date());

[LOG]: "Hello farzad, today is Sat Apr 24 2021!"

let msg = "hello there!";
//   با اینکه تایپ مسیج تعریف نشده تی اس خودکار استرینگ در نظر می گیرد

// فایل تی اس
// @showEmit
// @target: es5
function greet(person: string, date: Date) {
  console.log(`Hello ${person}, today is ${date.toDateString()}!`);
}

greet("Maddison", new Date());

// تبدیل شده به جی اس
"use strict";
// @showEmit
// @target: es5
function greet(person, date) {
    console.log("Hello " + person + ", today is " + date.toDateString() + "!");
}
greet("Maddison", new Date());


// مثال
html >
    lang;
"en" >
    charset;
"UTF-8" /  >
    name;
"viewport";
content = "width=device-width, initial-scale=1.0" /  >
    http - equiv;
"X-UA-Compatible";
content = "ie=edge" /  >
    Understanding;
TypeScript < /title>
    < script;
src = "using-ts.js";
defer > /script>
    < /head>
    < body >
    type;
"number";
id = "num1";
placeholder = "Number 1" /  >
    type;
"number";
id = "num2";
placeholder = "Number 2" /  >
    Add < /button>
    < /body>
    < /html>;
// جاوااسکریپت
var button = document.querySelector("button");
var input1 = document.getElementById("num1");
var input2 = document.getElementById("num2");
function add(num1, num2) {
    return num1 + num2;
}
button.addEventListener("click", function () {
    console.log(add(input1.value, input2.value));
});
//  input اول عدد 10 و در input دوم عدد 5  : 
// نتیجه میشه 105 نه 15 چون پیشفرض رشته هستن
// const input1 = document.getElementById("num1")! as HTMLInputElement;  علامت ! 
// مشخص کردن تایپ
function add(num1, num2) {
    return num1 + num2;
}
// تبدیل به عدد با +
button.addEventListener("click", function () {
    console.log(add(+input1.value, +input2.value));
});
// مفاهیم dynamic type و static type
//number :  -10و 5.6 و 6
//string :
// استفاده از علامت single quote (یعنی ') مانند 'Hi'
// استفاده از علامت double quote (یعنی ") مانند "Hi"
// استفاده از template literal (یعنی علامت backtic یا `) مانند `Hi`
function add(n1, n2) {
    return n1 + n2;
}
var number1 = 5;
var number2 = 2.8;
var result = add(number1, number2);
console.log(result);
var number1 = '5';
var number2 = 2.8;
// 52.8 
function add(n1, n2) {
    return n1 + n2;
}
function add(n1, n2) {
    return n1 + n2;
}
var number1 = 5;
var number1;
var number1;
number1 = '5'; // خطا
// ============================================
var person = {
    name: 'Maximilian',
    age: 30
};
console.log(person);
console.log(person.nickname); // خطا
// اضافه کردن تایپ شی
var person = {
    name: 'Maximilian',
    age: 30
};
var person = {
    name: 'Maximilian',
    age: 30
};
// ============================================
var person = {
    name: 'Maximilian',
    age: 30,
    hobbies: ['Sports', 'Cooking']
};
var person = {
    name: 'Maximilian',
    age: 30,
    hobbies: ['Sports', 'Cooking']
};
var favoriteActivities; // تعریف تایپ آرایه ای از نوع رشته
//Tuple
var person = {
    name: 'Maximilian',
    age: 30,
    hobbies: ['Sports', 'Cooking'],
    role: [2, 'author']
};
var person = {
    name: 'Maximilian',
    age: 30,
    hobbies: ['Sports', 'Cooking'],
    role: [2, 'author']
};
// enum
Enum;
name;
{
    NEW, OLD;
}
var person = {
    name: 'Maximilian',
    age: 30,
    hobbies: ['Sports', 'Cooking'],
    role: [2, 'author']
};
var Role;
(function (Role) {
    Role["ADMIN"] = "ADMIN";
    Role[Role["READ_ONLY"] = 100] = "READ_ONLY";
    Role["AUTHOR"] = "AUTHOR";
})(Role || (Role = {}));
;
(function (Role) {
    Role[Role["ADMIN"] = 0] = "ADMIN";
    Role[Role["READ_ONLY"] = 1] = "READ_ONLY";
    Role[Role["AUTHOR"] = 2] = "AUTHOR";
})(Role || (Role = {}));
;
(function (Role) {
    Role[Role["ADMIN"] = 5] = "ADMIN";
    Role[Role["READ_ONLY"] = 6] = "READ_ONLY";
    Role[Role["AUTHOR"] = 7] = "AUTHOR";
})(Role || (Role = {}));
; // میتونیم ایندکس را خودمان تعیین کنیم
(function (Role) {
    Role[Role["ADMIN"] = 0] = "ADMIN";
    Role[Role["READ_ONLY"] = 1] = "READ_ONLY";
    Role[Role["AUTHOR"] = 2] = "AUTHOR";
})(Role || (Role = {}));
;
var person = {
    name: 'Maximilian',
    age: 30,
    hobbies: ['Sports', 'Cooking'],
    role: Role.ADMIN
};
//Any
var favoriteActivities;
var favoriteActivities; // محدودتر کردیم گفتیم هر تایپی از نوع آرایه باشد
//Union Type
// مثال زیر فقط جمع اعداد رو قبول میکنه
function combine(input1, input2) {
    var result = input1 + input2;
    return result;
}
// میخوایم کاری کنیم هم با رشته هم عدد بتونه کار کنه
function combine(input1, input2) {
    var result = input1 + input2;
    return result;
}
function combine(input1, input2) {
    var result;
    if (typeof input1 === 'number' && typeof input2 === 'number') {
        result = input1 + input2;
    }
    else {
        result = input1.toString() + input2.toString();
    }
    return result;
}
//Literal Type
function combine(input1, input2, resultConversion) {
    var combinedAges = combine(30, 26, 'as-number');
    var combinedAges = combine(30, 26, 'as-number');
    console.log(combinedAges);
    var combinedStringAges = combine('30', '26', 'as-number');
    console.log(combinedStringAges);
    var combinedNames = combine('Max', 'Anna', 'as-text');
    console.log(combinedNames);
    function combine(input1, input2, resultConversion) {
        var result;
        if (typeof input1 === 'number' && typeof input2 === 'number') {
            result = input1 + input2;
        }
        else {
            result = input1.toString() + input2.toString();
        }
        if (resultConversion === 'as-number') {
            return +result;
        }
        else {
            return result.toString();
        }
    }
}

*/
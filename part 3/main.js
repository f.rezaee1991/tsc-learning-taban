var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
//=======================================================================
//--------------------
//class 
var Depatrment = /** @class */ (function () {
    function Depatrment(n) {
        this.name = n;
    }
    return Depatrment;
}());
console.log(typeof Depatrment);
var myAccount = new Depatrment('Accounting');
console.log(myAccount);
console.log(typeof myAccount);
/* LOG]: class Depatrment {
    constructor(n) {
        this.name = n;
    }
}
[LOG]: "function"
[LOG]: "function"
[LOG]: Depatrment: {
  "name": "Accounting"
}
[LOG]: "object"

*/
//--------------------
// add method 
var Depatrment = /** @class */ (function () {
    function Depatrment(n) {
        this.name = n;
    }
    Depatrment.prototype.describ = function () {
        console.log('Depatrment :' + this.name);
    };
    return Depatrment;
}());
var accounting = new Depatrment('Accounting');
accounting.describ();
//--------------------
var Depatrment = /** @class */ (function () {
    function Depatrment(n) {
        this.name = n;
    }
    Depatrment.prototype.describ = function () {
        console.log('Depatrment :' + this.name);
    };
    return Depatrment;
}());
var accounting = new Depatrment('Accounting');
// accounting.describ();
var accountingCopy = { describ: accounting.describ }; // یک آبجکت ساختیم ولی نه از روی کلاس
console.log(accountingCopy);
console.log(accountingCopy.describ());
/*

[LOG]: {}
[LOG]: "Depatrment :undefined"  // this بخاطر کلمه
[LOG]: undefined
*/
//--------------------
var Depatrment = /** @class */ (function () {
    function Depatrment(n) {
        this.name = n;
    }
    Depatrment.prototype.describ = function () {
        console.log('Depatrment :' + this.name);
    };
    return Depatrment;
}());
var accounting = new Depatrment('Accounting');
// accounting.describ();
var accountingCopy = { name: "sheyda", describ: accounting.describ };
accountingCopy.describ();
/*
[LOG]: "Depatrment :sheyda"

*/
//--------------------
var Depatrment = /** @class */ (function () {
    function Depatrment(n) {
        this.employees = [];
        this.name = n;
    }
    Depatrment.prototype.describ = function () {
        console.log('Depatrment :' + this.name);
    };
    Depatrment.prototype.addEmployee = function (employee) {
        this.employees.push(employee);
    };
    Depatrment.prototype.printEmployeesInformation = function () {
        console.log(this.employees.length);
        console.log(this.employees);
    };
    return Depatrment;
}());
var accounting = new Depatrment('Accounting');
accounting.addEmployee('Salar');
accounting.addEmployee('mehran modiri');
accounting.printEmployeesInformation();
//[LOG]: 2 
//[LOG]: ["Salar", "mehran modiri"] 
//--------------------
var Depatrment = /** @class */ (function () {
    function Depatrment(n) {
        this.employees = [];
        this.name = n;
    }
    Depatrment.prototype.describ = function () {
        console.log('Depatrment :' + this.name);
    };
    Depatrment.prototype.addEmployee = function (employee) {
        this.employees.push(employee);
    };
    Depatrment.prototype.printEmployeesInformation = function () {
        console.log(this.employees.length);
        console.log(this.employees);
    };
    return Depatrment;
}());
var accounting = new Depatrment('Accounting');
accounting.addEmployee('Salar');
accounting.addEmployee('mehran modiri');
// accounting.printEmployeesInformation();
accounting.employees[3] = "zahra"; // تونستیم بغیر از متد اضافه کردن به کارمندان به لیست کارمندان زهرا رو اضافه کنیم که این اصلا خوب نیست
accounting.printEmployeesInformation();
//[LOG]: 4 
//[LOG]: ["Salar", "mehran modiri", , "zahra"] 
//--------------------
var Depatrment = /** @class */ (function () {
    function Depatrment(n) {
        this.employees = []; // پرایویت کردیم
        this.name = n;
    }
    Depatrment.prototype.describ = function () {
        console.log('Depatrment :' + this.name);
    };
    Depatrment.prototype.addEmployee = function (employee) {
        this.employees.push(employee);
    };
    Depatrment.prototype.printEmployeesInformation = function () {
        console.log(this.employees.length);
        console.log(this.employees);
    };
    return Depatrment;
}());
var accounting = new Depatrment('Accounting');
accounting.addEmployee('Salar');
accounting.addEmployee('mehran modiri');
// accounting.printEmployeesInformation();
accounting.employees[3] = "zahra"; // دیگه دسترسی نداریم
accounting.printEmployeesInformation();
//Property 'employees' is private and only accessible within class 'Depatrment'.
//--------------------
var Depatrment = /** @class */ (function () {
    function Depatrment(id, name) {
        this.id = id;
        this.name = name;
        // private id : string;  // بنا به اصل درای که  کد رو تکرار نکنیم به جای اینجا توی کانستراکتور تعریف میکنیم
        // private name: string;
        this.employees = [];
        // this.id = id;
        // this.name = n;
    }
    Depatrment.prototype.describ = function () {
        console.log('Depatrment :' + this.name);
    };
    Depatrment.prototype.addEmployee = function (employee) {
        this.employees.push(employee);
    };
    Depatrment.prototype.printEmployeesInformation = function () {
        console.log(this.employees.length);
        console.log(this.employees);
    };
    return Depatrment;
}());
var accounting = new Depatrment('id-1', 'Accounting');
accounting.addEmployee('Salar');
accounting.addEmployee('mehran modiri');
// accounting.printEmployeesInformation();
accounting.printEmployeesInformation();
//[LOG]: 2   [LOG]: ["Salar", "mehran modiri"] 
//--------------------
var Depatrment = /** @class */ (function () {
    // میتونیم با  این کلمه اجازه ندیم متغیر تغییر کنه read only
    function Depatrment(id, name) {
        this.id = id;
        this.name = name;
        // private id : string;
        // private name: string;
        this.employees = [];
        // this.id = id;
        // this.name = n;
    }
    Depatrment.prototype.describ = function () {
        console.log('Depatrment :' + this.name);
    };
    Depatrment.prototype.addEmployee = function (employee) {
        this.id = 'id70000'; // نمیتونیم دیگه تغییر بدیم 
        this.employees.push(employee);
    };
    Depatrment.prototype.printEmployeesInformation = function () {
        console.log(this.employees.length);
        console.log(this.employees);
    };
    return Depatrment;
}());
var accounting = new Depatrment('id-1', 'Accounting');
accounting.addEmployee('Salar');
accounting.addEmployee('mehran modiri');
// accounting.printEmployeesInformation();
accounting.printEmployeesInformation();
//--------------------
var Depatrment = /** @class */ (function () {
    function Depatrment(id, name) {
        this.id = id;
        this.name = name;
        // private id : string;
        // private name: string;
        this.employees = [];
        // this.id = id;
        // this.name = n;
    }
    Depatrment.prototype.describ = function () {
        console.log('Depatrment :' + this.name);
    };
    Depatrment.prototype.addEmployee = function (employee) {
        this.employees.push(employee);
    };
    Depatrment.prototype.printEmployeesInformation = function () {
        console.log(this.employees.length);
        console.log(this.employees);
    };
    return Depatrment;
}());
var ItDepartment = /** @class */ (function (_super) {
    __extends(ItDepartment, _super);
    function ItDepartment(id, admins) {
        var _this = _super.call(this, id, 'information tecnology') || this;
        _this.admins = admins;
        return _this;
    }
    ItDepartment.prototype.addEmployee = function (employee) {
        this.employees.push(employee);
    };
    return ItDepartment;
}(Depatrment));
var d1 = new Depatrment('id-1', 'farzad Depatrment');
// d1.addEmployee('farzad');
// d1.printEmployeesInformation();
var it1 = new ItDepartment('id it-1', ['sahar', 'mahsa']);
it1.addEmployee('person 1');
console.log(it1);
//[LOG]: ItDepartment: {
// "id": "id it-1",
// "name": "information tecnology",
// "employees": [
//   "person 1"
// ],
// "admins": [
//   "sahar",
//  "mahsa"
//  ]
//--------------------
//static
var AnotherClass = /** @class */ (function () {
    function AnotherClass() {
        this.x = 5;
    }
    return AnotherClass;
}());
var xo = new AnotherClass();
console.log(xo);
/* [LOG]: AnotherClass: {}
[LOG]: AnotherClass: {
  "x": 5
}
*/
//--------------------
var AnotherClass = /** @class */ (function () {
    function AnotherClass() {
    }
    return AnotherClass;
}());
var AnotherClass2 = /** @class */ (function (_super) {
    __extends(AnotherClass2, _super);
    function AnotherClass2() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.x = 50;
        return _this;
    }
    return AnotherClass2;
}(AnotherClass));
console.log(AnotherClass2);
var user1 = {
    name: 'farzad',
    age: 25,
    greet: function (phrase) {
        console.log(phrase + " " + this.name);
    }
};
console.log(user1);
console.log(typeof user1); // object 
user1.greet('salam be aroosake kooki'); // [LOG]: "salam be aroosake kooki farzad"
var Person = /** @class */ (function () {
    function Person(name) {
        this.name = name;
        this.age = 78;
    }
    Person.prototype.greet = function (phrase) {
        console.log(phrase, this.name);
    };
    return Person;
}());
var user1 = new Person('Hosein');
user1.greet('dooroode AMOON bar to');
var e1 = {
    name: 'Farzad',
    privileges: ['create-server'],
    startDate: new Date()
};
//--------------------
var userInput = undefined;
var storedData = userInput !== null && userInput !== void 0 ? userInput : 'DEFAULT'; // اگه نال یا آندیفاین باشه سمت سمت چپ در غیر این صورت سمت راستی
console.log(storedData);
//--------------------
// const names: Array<string> = []; // string[]
// // names[0].split(' ');
// const promise: Promise<number> = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     resolve(10);
//   }, 2000);
// });
// promise.then(data => {
//   // data.split(' ');
// })
function merge(objA, objB) {
    return Object.assign(objA, objB);
}
var mergedObj = merge({ name: 'Farzad', hobbies: ['Sports'] }, { age: 30 });
console.log(mergedObj);
function countAndDescribe(element) {
    var descriptionText = 'Got no value.';
    if (element.length === 1) {
        descriptionText = 'Got 1 element.';
    }
    else if (element.length > 1) {
        descriptionText = 'Got ' + element.length + ' elements.';
    }
    return [element, descriptionText];
}
console.log(countAndDescribe(['Sports', 'Cooking']));
function extractAndConvert(obj, key) {
    return 'Value: ' + obj[key];
}
extractAndConvert({ name: 'Farzad' }, 'name');
var DataStorage = /** @class */ (function () {
    function DataStorage() {
        this.data = [];
    }
    DataStorage.prototype.addItem = function (item) {
        this.data.push(item);
    };
    DataStorage.prototype.removeItem = function (item) {
        if (this.data.indexOf(item) === -1) {
            return;
        }
        this.data.splice(this.data.indexOf(item), 1); // -1
    };
    DataStorage.prototype.getItems = function () {
        return __spreadArray([], this.data);
    };
    return DataStorage;
}());
var textStorage = new DataStorage();
textStorage.addItem('Farzad');
textStorage.addItem('shahla');
textStorage.removeItem('Farzad');
console.log(textStorage.getItems());
var numberStorage = new DataStorage();
// const objStorage = new DataStorage<object>();
// const FarzadObj = {name: 'Farzad'};
// objStorage.addItem(FarzadObj);
// objStorage.addItem({name: 'shahla'});
// // ...
// objStorage.removeItem(FarzadObj);
// console.log(objStorage.getItems());
//--------------------



//=======================================================================
//--------------------
//class 
class Depatrment {
  name: string;
  constructor(n: string) {
    this.name = n;
  }
}

console.log(typeof Depatrment);

const myAccount = new Depatrment('Accounting');
console.log(myAccount);
console.log(typeof myAccount);

/* LOG]: class Depatrment {
    constructor(n) {
        this.name = n;
    }
} 
[LOG]: "function" 
[LOG]: "function" 
[LOG]: Depatrment: {
  "name": "Accounting"
} 
[LOG]: "object"

*/
//--------------------
// add method 

class Depatrment {
  name: string;
  constructor(n: string) {
    this.name = n;
  }

  describ() {
    console.log('Depatrment :' + this.name)
  }
}

const accounting = new Depatrment('Accounting');
accounting.describ();


//--------------------

class Depatrment {
  name: string;
  constructor(n: string) {
    this.name = n;
  }

  describ() {
    console.log('Depatrment :' + this.name)
  }
}

const accounting = new Depatrment('Accounting');
// accounting.describ();

const accountingCopy = { describ: accounting.describ }; // یک آبجکت ساختیم ولی نه از روی کلاس
console.log(accountingCopy);
console.log(accountingCopy.describ());

/* 

[LOG]: {} 
[LOG]: "Depatrment :undefined"  // this بخاطر کلمه  
[LOG]: undefined 
*/
//--------------------
class Depatrment {
  name: string;
  constructor(n: string) {
    this.name = n;
  }

  describ(this: Depatrment) {
    console.log('Depatrment :' + this.name)
  }
}

const accounting = new Depatrment('Accounting');
// accounting.describ();

const accountingCopy = { name: "sheyda", describ: accounting.describ };
accountingCopy.describ();

/* 
[LOG]: "Depatrment :sheyda" 

*/

//--------------------

class Depatrment {
  name: string;
  employees: string[] = [];
  constructor(n: string) {
    this.name = n;
  }

  describ(this: Depatrment) {
    console.log('Depatrment :' + this.name)
  }
  addEmployee(employee: string) {
    this.employees.push(employee);
  }
  printEmployeesInformation() {
    console.log(this.employees.length);
    console.log(this.employees);
  }

}

const accounting = new Depatrment('Accounting');
accounting.addEmployee('Salar');
accounting.addEmployee('mehran modiri');
accounting.printEmployeesInformation();

//[LOG]: 2 
//[LOG]: ["Salar", "mehran modiri"] 

//--------------------
class Depatrment {
  name: string;
  employees: string[] = [];
  constructor(n: string) {
    this.name = n;
  }

  describ(this: Depatrment) {
    console.log('Depatrment :' + this.name)
  }
  addEmployee(employee: string) {
    this.employees.push(employee);
  }
  printEmployeesInformation() {
    console.log(this.employees.length);
    console.log(this.employees);
  }

}

const accounting = new Depatrment('Accounting');
accounting.addEmployee('Salar');
accounting.addEmployee('mehran modiri');
// accounting.printEmployeesInformation();

accounting.employees[3] = "zahra";  // تونستیم بغیر از متد اضافه کردن به کارمندان به لیست کارمندان زهرا رو اضافه کنیم که این اصلا خوب نیست
accounting.printEmployeesInformation();


//[LOG]: 4 
//[LOG]: ["Salar", "mehran modiri", , "zahra"] 

//--------------------
class Depatrment {
  name: string;  // اگر مشخص نکینم پابلیک هست یا پرایویت یا تروتکت خودکار پابلیک در نظر گرفته میشه
  private employees: string[] = [];  // پرایویت کردیم
  constructor(n: string) {
    this.name = n;
  }

  describ(this: Depatrment) {  // متد ها هم میتونن پرایویت یا پروتکت باشن
    console.log('Depatrment :' + this.name)
  }
  addEmployee(employee: string) {
    this.employees.push(employee);
  }
  printEmployeesInformation() {
    console.log(this.employees.length);
    console.log(this.employees);
  }

}

const accounting = new Depatrment('Accounting');
accounting.addEmployee('Salar');
accounting.addEmployee('mehran modiri');
// accounting.printEmployeesInformation();

accounting.employees[3] = "zahra";  // دیگه دسترسی نداریم
accounting.printEmployeesInformation();

//Property 'employees' is private and only accessible within class 'Depatrment'.


//--------------------
class Depatrment {
  // private id : string;  // بنا به اصل درای که  کد رو تکرار نکنیم به جای اینجا توی کانستراکتور تعریف میکنیم
  // private name: string;
  private employees: string[] = [];

  constructor(private id: string, private name: string) {
    // this.id = id;
    // this.name = n;
  }

  describ(this: Depatrment) {
    console.log('Depatrment :' + this.name)
  }
  addEmployee(employee: string) {
    this.employees.push(employee);
  }
  printEmployeesInformation() {
    console.log(this.employees.length);
    console.log(this.employees);
  }

}

const accounting = new Depatrment('id-1', 'Accounting');
accounting.addEmployee('Salar');
accounting.addEmployee('mehran modiri');
// accounting.printEmployeesInformation();
accounting.printEmployeesInformation();


//[LOG]: 2   [LOG]: ["Salar", "mehran modiri"] 

//--------------------
class Depatrment {
  // private id : string;
  // private name: string;
  private employees: string[] = [];
  // میتونیم با  این کلمه اجازه ندیم متغیر تغییر کنه read only
  constructor(private readonly id: string, private name: string) {
    // this.id = id;
    // this.name = n;
  }

  describ(this: Depatrment) {
    console.log('Depatrment :' + this.name)
  }
  addEmployee(employee: string) {
    this.id = 'id70000'; // نمیتونیم دیگه تغییر بدیم 
    this.employees.push(employee);
  }
  printEmployeesInformation() {
    console.log(this.employees.length);
    console.log(this.employees);
  }

}

const accounting = new Depatrment('id-1', 'Accounting');
accounting.addEmployee('Salar');
accounting.addEmployee('mehran modiri');
// accounting.printEmployeesInformation();
accounting.printEmployeesInformation();



//--------------------
class Depatrment {
  // private id : string;
  // private name: string;
  protected employees: string[] = [];

  constructor(private id: string, private name: string) {
    // this.id = id;
    // this.name = n;
  }

  describ(this: Depatrment) {
    console.log('Depatrment :' + this.name)
  }
  addEmployee(employee: string) {
    this.employees.push(employee);
  }
  printEmployeesInformation() {
    console.log(this.employees.length);
    console.log(this.employees);
  }

}

class ItDepartment extends Depatrment {
  constructor(id: string, public admins: string[]) {
    super(id, 'information tecnology');

  }
  addEmployee(employee: string) {
    this.employees.push(employee);

  }

}


const d1 = new Depatrment('id-1', 'farzad Depatrment');
// d1.addEmployee('farzad');
// d1.printEmployeesInformation();

const it1 = new ItDepartment('id it-1', ['sahar', 'mahsa']);
it1.addEmployee('person 1');
console.log(it1);

//[LOG]: ItDepartment: {
// "id": "id it-1",
// "name": "information tecnology",
// "employees": [
//   "person 1"
// ],
// "admins": [
//   "sahar",
//  "mahsa"
//  ]

//--------------------
//static


class AnotherClass {
  x: number = 5;


}
const xo = new AnotherClass();
console.log(xo);

/* [LOG]: AnotherClass: {} 
[LOG]: AnotherClass: {
  "x": 5
}
*/
//--------------------
abstract class AnotherClass {

  abstract x: number;


}

class AnotherClass2 extends AnotherClass {
  x: number = 50;

}

console.log(AnotherClass2);
/* 
[LOG]: class AnotherClass2 extends AnotherClass {
    constructor() {
        super(...arguments);
        this.x = 50;
    }
} 
*/
//--------------------
//   
//--------------------
// interface

interface Person {
  name: string;
  age: number;
  greet(phrase: string): void;

}

let user1: Person = {
  name: 'farzad',
  age: 25,
  greet(phrase: string) {
    console.log(phrase + " " + this.name)
  }
}

console.log(user1);
console.log(typeof user1); // object 

user1.greet('salam be aroosake kooki') // [LOG]: "salam be aroosake kooki farzad"

//--------------------

interface Ahvalporsi {
  name: string;
  greet(phrase: string): void;

}

class Person implements Ahvalporsi {
  age = 78;
  constructor(public name: string) {
  }
  greet(phrase: string) {
    console.log(phrase, this.name)
  }

}

const user1 = new Person('Hosein');
user1.greet('dooroode AMOON bar to');

//[LOG]: "dooroode AMOON bar to",  "Hosein" 


//--------------------

type Admin = {
  name: string;
  privileges: string[];
};

type Employee = {
  name: string;
  startDate: Date;
};

// interface ElevatedEmployee extends Employee, Admin {}

type ElevatedEmployee = Admin & Employee;

const e1: ElevatedEmployee = {
  name: 'Farzad',
  privileges: ['create-server'],
  startDate: new Date()
};

type Combinable = string | number;
type Numeric = number | boolean;

type Universal = Combinable & Numeric;

//--------------------
const userInput = undefined;

const storedData = userInput ?? 'DEFAULT'; // اگه نال یا آندیفاین باشه سمت سمت چپ در غیر این صورت سمت راستی


console.log(storedData);
//--------------------
// const names: Array<string> = []; // string[]
// // names[0].split(' ');

// const promise: Promise<number> = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     resolve(10);
//   }, 2000);
// });

// promise.then(data => {
//   // data.split(' ');
// })

function merge<T extends object, U extends object>(objA: T, objB: U) {
  return Object.assign(objA, objB);
}

const mergedObj = merge({ name: 'Farzad', hobbies: ['Sports'] }, { age: 30 });
console.log(mergedObj);

interface Lengthy {
  length: number;
}

function countAndDescribe<T extends Lengthy>(element: T): [T, string] {
  let descriptionText = 'Got no value.';
  if (element.length === 1) {
    descriptionText = 'Got 1 element.';
  } else if (element.length > 1) {
    descriptionText = 'Got ' + element.length + ' elements.';
  }
  return [element, descriptionText];
}

console.log(countAndDescribe(['Sports', 'Cooking']));

function extractAndConvert<T extends object, U extends keyof T>(
  obj: T,
  key: U
) {
  return 'Value: ' + obj[key];
}

extractAndConvert({ name: 'Farzad' }, 'name');

class DataStorage<T extends string | number | boolean> {
  private data: T[] = [];

  addItem(item: T) {
    this.data.push(item);
  }

  removeItem(item: T) {
    if (this.data.indexOf(item) === -1) {
      return;
    }
    this.data.splice(this.data.indexOf(item), 1); // -1
  }

  getItems() {
    return [...this.data];
  }
}

const textStorage = new DataStorage<string>();
textStorage.addItem('Farzad');
textStorage.addItem('shahla');
textStorage.removeItem('Farzad');
console.log(textStorage.getItems());

const numberStorage = new DataStorage<number>();

// const objStorage = new DataStorage<object>();
// const FarzadObj = {name: 'Farzad'};
// objStorage.addItem(FarzadObj);
// objStorage.addItem({name: 'shahla'});
// // ...
// objStorage.removeItem(FarzadObj);
// console.log(objStorage.getItems());
//--------------------


/*
function add(n1: number, n2: number, showResult: boolean, phrase: string) {

    const result = n1 + n2;
    if (showResult) {
        console.log(phrase + result);
    } else {
        return result;
    }
}

const number1 = 5; // 5.0
const number2 = 2.8;
const printResult = true;
const resultPhrase = 'Result is: ';

add(number1, number2, printResult, resultPhrase);

//--------------------
function add(n1: number, n2: number, showResult: boolean, phrase: string) {
    // if (typeof n1 !== 'number' || typeof n2 !== 'number') {
    //   throw new Error('Incorrect input!');
    // }
    const result = n1 + n2;
    if (showResult) {
        console.log(phrase + result);
    } else {
        return result;
    }
}

let number1: number;
number1 = 5;
// const n: string = 90; // نیاز نیست چون خودت تی اس انجام میده

// let n:boolean; // در اینجا بهتره تایپ مشخص شه
// n = true;
// let n ;
// n = 'sdfsdfsdf'  // تایپ any میشه چون مشخص نکردیم

const number2 = 2.8;   // در کانست  خود مقدار در نظر گرفته میشه
const printResult = true;
let resultPhrase = 'Result is: '; // در لت نوع مقدار در نظر گرفته میشه
//resultPhrase = 0;  ارور میده

add(number1, number2, printResult, resultPhrase);


//--------------------
// key : type در تی اس
// const person: {
//   name: string;
//   age: number;
// } = {
// const person = {
//   name: 'farzad',
//   age: 30
// };

// console.log(person.firstname); // خطا میده چون پروپرتی فرست نیم نداریم
// // Property 'firstname' does not exist on type '{ name: string; age: number; }'.

//------------
// const person :{ // تعریف تایپ آبجکت که بهتره استفاده نکنیم 
//   name:string;
// } = {
//   name: 'farzad',
//   age: 30
// };

// console.log(person.name); // 

//------------
const person = {
    name: 'farzad',
    age: 30
};

console.log(person.name); // 
//--------------------

//nested objects.

JavaScript object:

const product = {
    id: 'abc1',
    price: 12.99,
    tags: ['great-offer', 'hot-and-new'],
    details: {
        title: 'Red Carpet',
        description: 'A great carpet - almost brand-new!'
    }
}
type of such an object:

{
    id: string;
    price: number;
    tags: string[],
        details: {
        title: string;
        description: string;
    }
}

//--------------------
// const person: {
//   name: string;
//   age: number;
// } = {
const person = {
    name: 'Farzad
    age: 30,
    hobbies: ['Sports', 'Cooking', 'watch Perspolice football']
};


// let odds :number[];
// odds = [5,6,'jigar'] // خطا چون تایپ نامبر تعریف شده

let favoriteActivities: string[];
favoriteActivities = ['Sports'];

console.log(person.name);

for (const item of person.hobbies) {
    console.log(item);
}

// for (const hobby of person.hobbies) {
//   console.log(hobby.toUpperCase());
//   // console.log(hobby.map()); // !!! ERROR !!!  چون مپ برای آرایه هاست نه رشته ها
// }
//--------------------

// tuple نوع
// const t : (string| number)[]= [2,'hosein']; 
const t: [number, string] = [2, 'hosein'];  // tuple است
t.push('gholam'); // پوش استثنا است
t[0] = true;  // خطا 

//--------------------

enum Role { ADMIN = 'ADMIN', READ_ONLY = 100, AUTHOR = 'AUTHOR' };

const person = {
    name: 'Farzad
    age: 30,
    hobbies: ['Sports', 'Cooking'],
    role: Role.ADMIN
};

// person.role.push('admin');
// person.role[1] = 10;

// person.role = [0, 'admin', 'user'];

let favoriteActivities: string[];
favoriteActivities = ['Sports'];

console.log(person.name);

for (const hobby of person.hobbies) {
    console.log(hobby.toUpperCase());
    // console.log(hobby.map()); // !!! ERROR !!!
}

if (person.role === Role.AUTHOR) {
    console.log('is author');
}
//--------------------
function combine(input1: number | string, input2: number | string) {
    let result;
    if (typeof input1 === "number" && typeof input2 === "number") {
        result = input1 + input2;
    } else {
        result = input1.toString() + " " + input2.toString();
    }
    return result;

}

const combinedAges = combine(30, 26);
console.log(combinedAges);
const combinedName = combine('farzad', 'rezaei');
console.log(combinedName);
const combinedMix = combine('farzad', 15);
console.log(combinedMix);



//--------------------
function combine(
    input1: number | string,
    input2: number | string,
    resultConversion: 'as-number' | 'as-text'  // لیترال تایپ
) {
    let result;
    if (typeof input1 === 'number' && typeof input2 === 'number' || resultConversion === 'as-number') {
        result = +input1 + +input2;
    } else {
        result = input1.toString() + input2.toString();
    }
    return result;
    // if (resultConversion === 'as-number') {
    //   return +result;
    // } else {
    //   return result.toString();
    // }
}

const combinedAges = combine(30, 26, 'as-number');
console.log(combinedAges);

const combinedStringAges = combine('30', '26', 'as-number');
console.log(combinedStringAges);

const combinedNames = combine('Far', 'Anna', 'as-text');
console.log(combinedNames);


type OneToFive = 1 | 2 | 3 | 4 | 5;
type Bools = true | false;


//--------------------
// type aliases  تعریف 
type animal = 'cat' | 'dog';
let typeName: animal = 'cat';
let typeName2: animal = 'pishoo';

Type '"pishoo"' is not assignable to type 'animal
//--------------------
type User = { name: string; age: number };
const u1: User = { name: 'Far', age: 30 }; // this works!
This allows you to avoid unnecessary repetition and manage types centrally.

For example, you can simplify this code:

function greet(user: { name: string; age: number }) {
    console.log('Hi, I am ' + user.name);
}

function isOlder(user: { name: string; age: number }, checkAge: number) {
    return checkAge > user.age;
}
To:

type User = { name: string; age: number };

function greet(user: User) {
    console.log('Hi, I am ' + user.name);
}

function isOlder(user: User, checkAge: number) {
    return checkAge > user.age;
}
//--------------------
// function add(n1:number, n2: number) { // :void
//   console.log(n1 + n2);
// }

// console.log(add(15, 8));

function add(n1: number, n2: number) { // :number
    return n1 + n2;
}

console.log(add(15, 8));



//--------------------

function add(n1: number, n2: number) { // :string
    return n1.toString() + n2.toString();
}

console.log(add(15, 8)); // "158"
//--------------------


// function add(n1:number, n2: number) { 
//   return n1 + n2;
// }

// let c;
// c = add;
// c=2;
// console.log(c(4,8));


// function add(n1:number, n2: number) { 
//   return n1 + n2;
// }

// let c :Function;
// c = add;
// c=2; //ارور میده چون دیگه نامبر نیست
// console.log(c(4,8));

// function add(n1:number, n2: number) { 
//   return n1 + n2;
// }

// let c :Function;
// c = isFinite; // یک تابع است ولی تابعی با شرایطی که ما میخوایم نیست


function add(n1: number, n2: number) {
    return n1 + n2;
}

let c: (a: number, b: number) => number;
c = add; // 
console.log(c(4, 8)); // 12


//--------------------
//فانکشن callback
function addAndHandle(n1: number, n2: number, cb: (num: number) => void) {
    const result = n1 + n2;
    cb(result);
}


addAndHandle(10, 20, (result) => {
    console.log(result);
});
//--------------------


// const add = (x: number, y: number) => x + y;

// console.log(add(6, 7));

const userName = 'Far';
// userName = 'Farzad
let age = 30;

age = 29;

// function add(a: number, b: number) {
//   let result;
//   result = a + b;
//   return result;
// }

// if (age > 20) {
//   let isOld = true;
// }

// console.log(isOld);

// console.log(result);

const add = (a: number, b: number) => a + b;

const printOutput: (a: number | string) => void = output => console.log(output);

const button = document.querySelector('button');

if (button) {
    button.addEventListener('click', event => console.log(event));
}

printOutput(add(5, 2));

//--------------------
const hobbies = ['Sports', 'Cooking'];
const activeHobbies = ['Hiking'];
const allArrs = [...activeHobbies, ...hobbies]
console.log(allArrs);

activeHobbies.push(...hobbies);

const person = {
    name: 'Farzad',
    age: 20
};

const copiedPerson = { ...person };
console.log(person);
console.log(copiedPerson);
console.log(Object.is(person, copiedPerson)); // false 
//--------------------


const person = {
    firstName: 'Far',
    age: 30
};

const copiedPerson = { ...person };

const add = (...numbers: number[]) => {
    return numbers.reduce((curResult, curValue) => {
        return curResult + curValue;
    }, 0);
};


const { firstName: userName, age } = person;

console.log(userName, age, person);
*/
    //--------------------
